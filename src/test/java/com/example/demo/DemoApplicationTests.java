package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class DemoApplicationTests {

	@Test
	void contextLoads() {
	}

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	void homeResponse() {
		String body = this.restTemplate.getForObject("/", String.class);
		assertEquals("Spring is here!", body);
	}

	@Test
    public void testSayHello() {
        // Sende eine GET-Anfrage an /hello
        ResponseEntity<String> response = restTemplate.getForEntity("/hello", String.class);

        // Überprüfe, ob der Statuscode 200 OK ist
        assertEquals(HttpStatus.OK, response.getStatusCode());

        // Überprüfe, ob die Antwort "Hello, World!" enthält
        assertNotNull(response.getBody());
        assertEquals("Hello, World!", response.getBody());
    }
}
